# piano-convertor

自制调音小工具，根据简谱和基准音声调来自动计算出其他音的声调

# 使用方法
- 1.在右上角点击下载，并选择download zip （或直接 git clone）
- 2.打开压缩包中的piano.html 并按照视频中的方法使用。（另一个.css结尾的文件是“网页皮肤包”，勿删）

# 视频演示
- https://www.bilibili.com/video/av31349500/
- https://youtu.be/u6wZ8T8Ds-w